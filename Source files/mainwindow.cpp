#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    if (objectName().isEmpty())
                setObjectName(QString::fromUtf8("MainWindow"));
    resize(340, 345);

    action = new QAction(this);
    action->setObjectName(QString::fromUtf8("action"));
    action_2 = new QAction(this);
    action_2->setObjectName(QString::fromUtf8("action_2"));
    action_3 = new QAction(this);
    action_3->setObjectName(QString::fromUtf8("action_3"));

    centralwidget = new QWidget(this);
    centralwidget->setObjectName(QString::fromUtf8("centralwidget"));

    vertLayout = new QVBoxLayout(centralwidget);
    vertLayout->setObjectName(QString::fromUtf8("verticalLayout"));
    vertLayout->setContentsMargins(0, 0, 0, 0);

    pushButton = new QPushButton(centralwidget);
    pushButton->setObjectName(QString::fromUtf8("pushButton"));

    vertLayout->addWidget(pushButton);

    setCentralWidget(centralwidget);

    menubar = new QMenuBar(this);
    menubar->setObjectName(QString::fromUtf8("menubar"));
    menubar->setGeometry(QRect(0, 0, 340, 21));
    menu = new QMenu(menubar);
    menu->setObjectName(QString::fromUtf8("menu"));
    setMenuBar(menubar);
    statusbar = new QStatusBar(this);
    statusbar->setObjectName(QString::fromUtf8("statusbar"));
    setStatusBar(statusbar);

    menubar->addAction(menu->menuAction());
    menu->addAction(action);
    menu->addAction(action_2);
    menu->addSeparator();
    menu->addAction(action_3);

    setWindowTitle(QCoreApplication::translate("MainWindow", "\320\223\321\200\320\260 \320\262 15", nullptr));
    action->setText(QCoreApplication::translate("MainWindow", "&\320\235\320\276\320\262\320\260 \320\263\321\200\320\260", nullptr));
    action_2->setText(QCoreApplication::translate("MainWindow", "&\320\237\321\200\320\276 \320\263\321\200\321\203", nullptr));
    action_3->setText(QCoreApplication::translate("MainWindow", "&\320\222\320\270\320\271\321\202\320\270", nullptr));
    pushButton->setText(QCoreApplication::translate("MainWindow", "&\320\235\320\276\320\262\320\260 \320\263\321\200\320\260", nullptr));
    menu->setTitle(QCoreApplication::translate("MainWindow", "\320\223\321\200\320\260", nullptr));

    QMetaObject::connectSlotsByName(this);
    connect(action, SIGNAL(triggered()), this, SLOT(deleteNewGame()));
    connect(action_2, SIGNAL(triggered()), this, SLOT(action_2_triggered()));
    connect(action_3, SIGNAL(triggered()), this, SLOT(close()));
    connect(pushButton, SIGNAL(clicked()), this, SLOT(deleteNewGame()));

    newGame();
}

MainWindow::~MainWindow()
{
    vertLayout->removeWidget(table);
    delete table;

    menu->removeAction(action_3);
    menu->removeAction(action_2);
    menu->removeAction(action);
    menubar->removeAction(menu->menuAction());
    vertLayout->removeWidget(pushButton);

    delete action;
    delete action_2;
    delete action_3;
    delete menu;
    delete menubar;
    delete pushButton;
    delete vertLayout;
    delete centralwidget;
}

void MainWindow::newGame()
{
    table = new GameTable(centralwidget);
    vertLayout->addWidget(table);
    vertLayout->update();
    table->hide();
    table->show();
}

void MainWindow::deleteNewGame()
{
    vertLayout->removeWidget(table);
    delete table;
    newGame();
}

void MainWindow::action_2_triggered()
{
    QMessageBox::information(this, "message", "Курсова робота \"Гра в 15\"\nВиконав: Геник Михайло Ігорович, СП-21\nВерсія: 1.0");
}
