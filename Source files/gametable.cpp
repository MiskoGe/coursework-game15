#include "gametable.h"
#include "mainwindow.h"
#include <QtGlobal>
#include <QGridLayout>
#include <QPoint>
#include <QPushButton>
#include <QTime>
#include "QMessageBox"
#include <cstdlib>

GameTable::GameTable(QWidget *parent) : QWidget(parent)
{
    layout = new QGridLayout;
    layout->setContentsMargins(2, 2, 2, 2);

    this->setWinCondition();
    this->addButtonsOnField();
    this->setLayout(layout);
    this->shuffleButtons();
}

GameTable::~GameTable(){}

void GameTable::setWinCondition() {
    for (int i = 1; i < 16; i++) {
        this->winCondition << QString::number(i);
    }
    this->winCondition << "0";
    qDebug() << winCondition;
}

void GameTable::addButtonsOnField() {
    int buttonNumber = 1;

    for (int row = 0; row < 4; row++) {
        for (int column = 0; column < 4; column++) {
            if (column == 3 && row == 3) {
                btn = new QPushButton("0");
                btn->setMinimumSize(60, 55);

                connect(btn, SIGNAL(clicked()), this, SLOT(handleButton()));

                layout->addWidget(btn, 3, 3);
                btn->setVisible(false);
            } else {
                QString buttonName = QString::number(buttonNumber);

                btn = new QPushButton(buttonName);
                btn->setMinimumSize(60, 60);
                connect(btn, SIGNAL(clicked()), this, SLOT(handleButton()));

                layout->addWidget(btn, row, column);
                buttonNumber++;
            }
        }
    }
}

void GameTable::shuffleButtons() {
    int fieldSquare = 16;
    QList<int> rndNums;
    srand((unsigned int)time(nullptr));

    for (int i = 0; i < 100; i++) {
        int randomNumber = rand() % fieldSquare;
        rndNums << randomNumber;
        QPushButton* randomButton = (QPushButton*)layout->itemAt(randomNumber)->widget();
        emit randomButton->clicked();
    }
    this->shuffling = false;
}

void GameTable::handleButton() {
    QPushButton* senderObject = (QPushButton*)sender();
    QPair<int, int> senderCoords = this->getButtonCoords(senderObject);
    bool isSwapped = false;
    int counter = 0;
    QLayoutItem* itemTop = layout->itemAtPosition(senderCoords.first - 1, senderCoords.second);
    QLayoutItem* itemRight = layout->itemAtPosition(senderCoords.first, senderCoords.second + 1);
    QLayoutItem* itemBottom = layout->itemAtPosition(senderCoords.first + 1, senderCoords.second);
    QLayoutItem* itemLeft = layout->itemAtPosition(senderCoords.first, senderCoords.second - 1);

    QList<QLayoutItem*> items;
    items << itemTop << itemRight << itemBottom << itemLeft;

    while (!isSwapped && counter != 4) {
        isSwapped = this->checkButton(senderObject, items[counter]);
        counter++;
    }

    if (!this->shuffling && this->isWinner()) {
        QMessageBox win;
        win.setText("Ви перемогли!");
        win.setIcon(QMessageBox::Information);
        win.setWindowTitle("Перемога");
        win.exec();
    }
}

bool GameTable::checkButton(QPushButton* sender, QLayoutItem *item) {
    if (item != nullptr) {
        QPushButton* tmpBtn = (QPushButton*)item->widget();
        if (tmpBtn->text() == "0") {
            this->swapButtons(sender, tmpBtn);
            return true;
        }
        return false;
    }
    return false;
}

void GameTable::swapButtons(QPushButton *source, QPushButton *target) {
    QPair<int, int> sourceCoords = this->getButtonCoords(source);
    QPair<int, int> targetCoords = this->getButtonCoords(target);

    layout->addWidget(source, targetCoords.first, targetCoords.second);
    layout->addWidget(target, sourceCoords.first, sourceCoords.second);
}

QPair<int, int> GameTable::getButtonCoords(QPushButton *sender) {
    int index = layout->indexOf(sender);
    QPair<int, int> coords;
    if(index != -1)
    {
        int row, col, col_span, row_span;
        layout->getItemPosition(index, &row, &col, &col_span, &row_span);

        coords.first = row;
        coords.second = col;
    }
    return coords;
}

bool GameTable::isWinner() {
    QList<QString> items;
    for (int row = 0; row < 4; row++) {
        for (int column = 0; column < 4; column++) {
            QPushButton* tmpBtn = (QPushButton*)layout->itemAtPosition(row, column)->widget();
            QString buttonName = tmpBtn->text();
            items << buttonName;
        }
    }
    qDebug() << items;
    if (items == this->winCondition) {
        return true;
    }
    return false;
}
