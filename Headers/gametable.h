#ifndef GAMETABLE_H
#define GAMETABLE_H

#include <QWidget>
#include <QList>
#include <QLayout>
#include <QPushButton>
#include <QDebug>
#include <QMessageBox>
#include <ctime>
#include "IGameable.h"
class QGridLayout;
class QPoint;
class QPushButton;

class GameTable : public QWidget, public IGameable
{
    Q_OBJECT
private slots:
    void handleButton();
private:
    bool shuffling = true;
    QList<QString> winCondition;

    QPushButton* btn;
    QPushButton* nullBtn;
    QGridLayout* layout;

    bool checkButton(QPushButton* sender, QLayoutItem* item);
    bool isWinner();
    QPair<int, int> getButtonCoords(QPushButton* sender);
    void addButtonsOnField();
    void shuffleButtons();
    void setWinCondition();
    void swapButtons(QPushButton* source, QPushButton *target);
public:
    explicit GameTable(QWidget *parent = nullptr);
    ~GameTable();
    void createButton(QPushButton*, int, int, int);
};

#endif // GAMETABLE_H
