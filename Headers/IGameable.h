#ifndef IGAMEABLE_H
#define IGAMEABLE_H

__interface IGameable
{
    bool isWinner();
    void setWinCondition();
};

#endif // IGAMEABLE_H
