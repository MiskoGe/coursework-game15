#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "QMessageBox"
#include "gametable.h"
#include <QVariant>
#include <QAction>
#include <QApplication>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QMenu>
#include <QMenuBar>
#include <QPushButton>
#include <QStatusBar>
#include <QWidget>
#include <QMainWindow>

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    QAction *action;
    QAction *action_2;
    QAction *action_3;
    QWidget *centralwidget;
    QPushButton *pushButton;
    QVBoxLayout *vertLayout;
    QMenuBar *menubar;
    QMenu *menu;
    QStatusBar *statusbar;

    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
private:
    GameTable* table;
public slots:
    void deleteNewGame();
    void newGame();
private slots:
    void action_2_triggered();
};
#endif // MAINWINDOW_H
